<?php

error_reporting(E_ALL | E_STRICT);

$PROJECTS_URL = 'https://projects.blender.org/';

$tasks_to_repo_json = file_get_contents('tasks-to-org-repos.json');
$tasks_to_repo = json_decode($tasks_to_repo_json, true)['tasks_to_repo'];

$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

if ($id && array_key_exists($id, $tasks_to_repo)) {
  $repo = $tasks_to_repo[$id];
  header("Location: {$PROJECTS_URL}{$repo}/issues/{$id}");
} else {
  header("Location: {$PROJECTS_URL}");
}

?>
